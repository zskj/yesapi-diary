import Vue from 'vue'
import App from './App'
import store from './store/index.js'
import {
	myRequest
} from './utils/api.js'


//顶部导航
import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom', cuCustom)
//首页
import home from './pages/home/home.vue'
Vue.component('home', home)
//书写
import write from './pages/write/write.vue'
Vue.component('write', write)
//我的
import my from './pages/my/my.vue'
Vue.component('my', my)
Vue.config.productionTip = false
Vue.prototype.$myRuquest = myRequest
//判断是否登录
Vue.prototype.checkLogin = function(backpage, backtype) {
	var userInfo = uni.getStorageSync('userInfo');
	if (userInfo == '') {
		uni.redirectTo({
			url: "/pages/login/login?backpage=" + backpage + "&backtype=" + backtype
		});
		return false;
	}
	return userInfo;
}
App.mpType = 'app'

const app = new Vue({
	...App,
	store
})
app.$mount()
