> 官网: hommm.com

> 作者: zskj

> 邮箱: xg.zskj@gmail.com

>安卓安装：[点击下载](http://diary.1tj.vc/101.apk)

## 预览
![image](doc/3.jpg)
![image](doc/4.jpg)
![image](doc/2.jpg)
![image](doc/1.jpg)

## 概述
本应用是一个简单的日记App
后端采用 [果创云接口](http://open.yesapi.cn/)
客户端使用 [uniapp](https://www.dcloud.io/)
## 使用
果创云后台（APP_KEY，APP_SECRECT，BASE_URL【域名】）相关参数在 components\okayapi\okayapi.js  文件中修改。
接口使用果创云云会员、以及果创云提供的迷你朋友圈项目+创建云函数
## 致谢
果创云
dcloud
uniapp相关插件开发者
## 更新日志
### 1.0.0(2020-12-14)
包含注册、登录、登出、写日记 、修改会员头像，密码等资料。公开日记列表，及个人日记日历展示。
* 完成第一个版本
