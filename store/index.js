import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		/**
		 * 是否需要强制登录
		 */
		tabpage: 'home',
		tabshow: true,
		forcedLogin: true,
		hasLogin: false,
		userInfo: {
			ext_info: {
				yesapi_avatar: "",
				yesapi_email: "",
				yesapi_expiration_time: "",
				yesapi_mobile: "",
				yesapi_nickname: "",
				yesapi_points: 0,
				yesapi_real_name: "",
				yesapi_reg_note: "",
				yesapi_reg_source: "",
				yesapi_sex: ""
			},
			register_ip: "",
			register_time: "",
			role: "",
			rolename: "",
			status: 0,
			username: "",
			uuid: "",
			token: ""
		},
	},
	mutations: {
		setData(state, param) { //param:{key:*,val:*}
			var val = param.val;
			if (typeof param.val == "object") {
				val = JSON.stringify(val);
			}
			eval(`state.${param.key}=val`);

		},
		login(state, provider) {
			//console.log(provider)
			state.userInfo = provider;
			state.hasLogin = true;
			uni.setStorageSync('userInfo', provider)
		},
		logout(state) {
			console.log("111")
			state.hasLogin = false;
			state.userInfo = {};
			uni.removeStorageSync('userInfo');
			state.tabpage = 'home';
			uni.reLaunch({
				url: "/pages/index"
			});
		}
	}
})

export default store
